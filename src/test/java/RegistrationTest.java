import Account.RegistrationUkranianPage;
import Other.OrderProcPage;
import Account.RegistrationPage;
import Utils.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by Home on 12.10.2017.
 */
public class RegistrationTest {
  static WebDriver driver;
  private static MyWaits wait;
  @BeforeClass
  void setup() {
    //System.setProperty("webdriver.chrome.driver", "C:\\Users\\Yuliya\\Desktop\\setup\\chromedriver.exe");
    System.setProperty("webdriver.chrome.driver", "C:\\Users\\Home\\Downloads\\chromedriver.exe");
    // System.setProperty("webdriver.chrome.driver", "C:\\Users\\it-school\\Desktop\\setup\\chromedriver.exe");
    driver = (WebDriver) new ChromeDriver();
    // driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), DesiredCapabilities.chrome());
    driver.get("https://www.templatemonster.com/");
    driver.manage().window().maximize();
    driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    wait = new MyWaits(driver);
  }
  @BeforeMethod
  void buy() {
    OrderProcPage buypr = PageFactory.initElements(driver, OrderProcPage.class);
    buypr.chekout();
  }
  @Test(dataProvider = "data-provider", dataProviderClass = DataPrForRegistration.class)
  void registrationTest(String path){
    RegistrationPage reg = PageFactory.initElements(driver, RegistrationPage.class);
    User user=new User(path);
    reg.registerAccount(user);
    wait.waitPayment();
    Assert.assertTrue(driver.findElement(By.xpath(".//*[@id='payment-methods-container']/div[1]")).isDisplayed());
  }

//  public void screenshotFailure(String filename) throws IOException {
//    File srcFile=driver.getScreenshotAs(OutputType.FILE);
//    File targetFile=new File("./Screenshots/Failure/" + manager.helperBase.generateCurrentDate() + "/" + filename +".jpg");
//    FileUtils.copyFile(srcFile,targetFile);
//  }
  @AfterClass
  public void tearDown() {
  driver.manage().deleteAllCookies();

    driver.quit();
  }
  }

