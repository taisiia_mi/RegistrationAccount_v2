package Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by it-school on 16.10.2017.
 */
public class Click {
  MyWaits myWaits;
  private final WebDriver driver;
  public Click(WebDriver driver) {
    this.driver = driver;
    myWaits = new MyWaits(driver);
  }
  public void methodClick(WebElement element)  {
    myWaits.waitElementClickable(element);
    element.click();
  }

}
